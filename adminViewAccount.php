<?php 
    session_start();
    //redirect to log in page if not logged in
    if (!isset($_SESSION['loggedIN']))
	{    
        header('Location: login.php');
        exit();
    }
    
    //logs out if tried to visit from wrong userType
    if($_SESSION['userType'] != "admin")
	{
        header('Location: logout.php');
        exit();
    }
    //logs out if tried to visit before searching for user
    if (!isset($_SESSION['user_id']))
	{    
        header('Location: logout.php');
        exit();
    }
    //Go back to AdminHome page
    if (isset($_POST['back']))
	{
		unset($_SESSION['user_user_id']);
		unset($_SESSION['user_password']);
		unset($_SESSION['user_NRIC']);
		unset($_SESSION['user_name']);
		unset($_SESSION['user_DOB']);
		unset($_SESSION['user_email']);
		unset($_SESSION['user_phone']);
		unset($_SESSION['user_role']);
		if($_SESSION['user_role'] == 'doctor')
		{
			unset($_SESSION['doctor_doctor_id']);
			unset($_SESSION['doctor_license']);
		}
		elseif($_SESSION['user_role'] == 'patient')
		{
			unset($_SESSION['patient_patient_id']);
			unset($_SESSION['patient_weight']);
			unset($_SESSION['patient_height']);
			unset($_SESSION['patient_gender']);
			unset($_SESSION['patient_address']);
			unset($_SESSION['patient_allergy']);
		}
		elseif($_SESSION['user_role'] == 'pharmacist')
		{
			unset($_SESSION['pharmacist_pharmacist_id']);
			unset($_SESSION['pharmacist_license']);
		}
		else
		{
			unset($_SESSION['admin_admin_id']);
		}
		exit('back');
    }
	if (isset($_POST['edit']))
	{
        $_SESSION['edit_id'] = $_POST['editIDPHP']; 
        exit('editSuccess');
	}

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
    <link rel="stylesheet" href=".\css\main.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            console.log('page ready: adminViewDoctor');

            $("#backButton").on('click', function (){

               $.ajax(
                    {
                        url: 'adminViewAccount.php',
                        method: 'POST',
                        data:{
                            back: 1
                        },
                        success: function(response){
                            $("#response").html(response);

                            if(response.indexOf('back') >= 0){

                                window.location = 'adminHome.php';
                                
                            }else{
                                alert("Please try again");
                            }                            
                        },dataType: 'text'
                    }
               );
            });
							
			$("#editAccountButton").on('click', function (){ 
                var editID = $_SESSION['user_id'];
				$.ajax(
				{
					url: 'adminViewAccount.php',
					method: 'POST',
					data:{
						edit: 1,
						editIDPHP: editID
					},
					success: function(response){
						$("#response").html(response);

						if(response.indexOf('editSuccess') >= 0){

							window.location = 'adminEditAccount.php';
							 
						}else{
							alert("Please try again");
						}
					 },dataType: 'text'
				});
		
			});
        });

        </script>
    
    
</head>
<body>
	<header>
		<nav>
			<img src="img\admin-with-cogwheels.png" alt="admin" style="width:7%; float: left;">
			<div class="nav__name"><h2><?php echo("{$_SESSION['name']}"); ?></h2></div>
        <ul>
            <li><a href="logout.php">Log out</a></li>
        </ul>
		</nav>
	</header>
	
	</br></br>
	<div>
		<button type="button" class="back__button" id="backButton">Back</button>
    </div>
	</br></br>
	 <div style="margin-left: 20%;">
		<table class = "table__add" style="width: 100%;">
				<tr>	
					<th style="width: 30%;">User ID</th>
					<td><?php echo("{$_SESSION['user_user_id']}"); ?></td>
				</tr>
				<tr>	
					<th style="width: 30%;">Password</th>
					<td><?php echo("{$_SESSION['user_password']}"); ?></td>
				</tr>
				<tr>	
					<th style="width: 30%;">NRIC</th>
					<td><?php echo("{$_SESSION['user_NRIC']}"); ?></td>
				</tr>
				<tr>	
					<th style="width: 30%;">Name</th>
					<td><?php echo("{$_SESSION['user_name']}"); ?></td>
				</tr>
				<tr>	
					<th style="width: 30%;">Date Of Birth</th>
					<td><?php echo("{$_SESSION['user_DOB']}"); ?></td>
				</tr>
				<tr>	
					<th style="width: 30%;">Email Address</th>
					<td><?php echo("{$_SESSION['user_email']}"); ?></td>
				</tr>
				<tr>	
					<th style="width: 30%;">Phone Number</th>
					<td><?php echo("{$_SESSION['user_phone']}"); ?></td>
				</tr>
				<tr>	
					<th style="width: 30%;">Role</th>
					<td><?php echo("{$_SESSION['user_role']}"); ?></td>
				</tr>  

				<tr><td></td>
					<td><button id="editAccountButton" class="edit__button--account" type="button"><a href="adminEditAccount.php">Edit Account</a></button>
				</tr>
			</table>
	</div>

    
</body>
</html>