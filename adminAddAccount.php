<?php 
    session_start();
	//redirect to log in page if not logged in
    if (!isset($_SESSION['loggedIN']))
	{
        header('Location: login.php');
        exit();
    }
    
    //logs out if tried to visit from wrong userType
    if($_SESSION['userType'] != "admin")
	{
        header('Location: logout.php');
        exit();
    }

	//generate a random token of strings
    function generatePwd()
	{
      $pwdLength = 10;
      $string = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFEHIJKLMNOPQRSTUVWXYZ";
      $randString = substr(str_shuffle($string),0,$pwdLength);
      return $randString;
    }
    $userPwd = generatepwd();
	
    if (isset($_POST['submitPHP']))
	{
        $connection = new mysqli('localhost', 'root', '', 'testestdb');

        $userNRIC 	= strtoupper($_POST['NRICPHP']);
        $userName 	= $_POST['namePHP'];
        $userDOB 	= $_POST['DOBPHP'];
		$userEmail 	= $_POST['emailPHP'];
        $userPhone 	= $_POST['phonePHP'];
        $userRole 	= $_POST['rolePHP'];
		
		$verify = $connection->query("SELECT * FROM user WHERE NRIC = $userNRIC");

		if($verify->num_rows == 0)
		{
			if($connection->query("INSERT INTO user(user_id,password,NRIC,name,DOB,email,phone_number,role) VALUES (NULL,'$userPwd','$userNRIC','$userName','$userDOB','$userEmail','$userPhone','$userRole')") === TRUE)
			{
				$getUser = $connection->query("SELECT * FROM user WHERE NRIC = '$userNRIC'");
				
				$userRow = $getUser->fetch_assoc();
				
				$_SESSION['user_user_id'] 	= $userRow['user_id'];
				$_SESSION['user_password'] 	= $userRow['password'];
				$_SESSION['user_NRIC'] 		= $userRow['NRIC'];
				$_SESSION['user_name'] 		= $userRow['name'];
				$_SESSION['user_DOB'] 		= $userRow['DOB'];
				$_SESSION['user_email'] 	= $userRow['email'];
				$_SESSION['user_phone'] 	= $userRow['phone_number'];
				$_SESSION['user_role'] 		= $userRow['role'];
				
				$userID = $userRow['user_id'];

				if($userRole == 'doctor'){
					if($connection->query("INSERT INTO doctor(doctor_id,user_id) VALUES (NULL,'$userID')") === TRUE){
						exit('submitSuccess');
					}else{
						exit('failed');
					}
				}elseif($userRole == 'patient'){
					if($connection->query("INSERT INTO patient(patient_id,user_id) VALUES (NULL,'$userID')") === TRUE){
						exit('submitSuccess');
					}else{
						exit('failed');
					}
				}elseif($userRole == 'pharmacist'){
					if($connection->query("INSERT INTO pharmacist(pharmacist_id,user_id) VALUES (NULL,'$userID')") === TRUE){
						exit('submitSuccess');
					}else{
						exit('failed');
					}
				}elseif($userRole == 'admin'){
					if($connection->query("INSERT INTO admin(admin_id,user_id) VALUES (NULL,'$userID')") === TRUE){
						exit('submitSuccess');
					}else{
						exit('failed');
					}
				}else{
					exit('failed');
				}
			}else{
				exit('exists');
			}
		}
    }
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
    <link rel="stylesheet" href=".\css\main.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
			
            console.log('page ready: AdminViewAccount');
			
			 $("#backButton").on('click', function (){

               $.ajax(
                    {
                        url: 'adminAddAccount.php',
                        method: 'POST',
                        data:{
                            back: 1
                        },
                        success: function(response){
                            $("#response").html(response);

                            if(response.indexOf('back') >= 0){

                                window.location = 'adminHome.php';
                                
                            }else{
                                alert("Please try again");
                            }
                            
                        },
                        dataType: 'text'
                    }
               );
            });
			
            $("#submitButton").on('click', function(event){  

                var NRIC = $("#user--NRIC").val();
                var name = $("#user--name").val();
                var DOB = $("#user--DOB").val();
                var email = $("#user--email").val();
                var phone = $("#user--phone").val();
				var role = $("input[name='user--role']:checked").val();
                
                $.ajax(
                    {
                    url: 'adminAddAccount.php',
                    method: 'POST',
                    data:{
                        submitPHP: 1,
                        NRICPHP: NRIC,
                        namePHP: name,
                        DOBPHP: DOB,
                        emailPHP: email,
                        phonePHP: phone,
						rolePHP: role
                    },
                    success: function(response){
                        $("#response").html(response);

                        if(response.indexOf('submitSuccess') >= 0){
                            window.location = 'adminViewAccount.php';
                        }else{
                            alert("Please try again");
                        }
                    },dataType: 'text'
                });
            });
                
        });

        </script>
    
</head>
<body>
    <nav>
        <img src="img\admin-with-cogwheels.png" alt="admin" style="width:7%">
        <div class="nav__name"><h2> <?php echo("{$_SESSION['name']}"); ?></h2></div>
        <ul>
            <li><a href="logout.php">Log out</a></li>
        </ul>
    </nav>

    <button type="button" class="back__button" id="backButton">Back</button>

	<div class="container">
		<div class="box1">
			<div class="input__Text">NRIC : </div>
			<div class="input__Text">Name : </div>
			<div class="input__Text">Date Of Birth : </div>
			<div class="input__Text">Email Address : </div>
			<div class="input__Text">Phone Number : </div>
			<div class="input__Text">Role : </div>
		</div>
		<div class="box2">
			<form method="POST">    
				<input type="text" name="user--NRIC" class="medicine__input" id="user--NRIC"required="required" >
				<input type="text" name="user--name" class="medicine__input" id="user--name"required="required">
				<input type="date" name="user--DOB" class="medicine__input" id="user--DOB"required="required">
				<input type="text" name="user--email" class="medicine__input" id="user--email"required="required">
				<input type="text" name="user--phone" class="medicine__input" id="user--phone"required="required"></br></br>
				
				<input type="radio" name="user--role" class="user__input" id="user--role" value="doctor"></input>Doctor
				<input type="radio" name="user--role" class="user__input" id="user--role" value="patient"></input>Patient
				<input type="radio" name="user--role" class="user__input" id="user--role" value="pharmacist"></input>Pharmacist
				<input type="radio" name="user--role" class="user__input" id="user--role" value="admin"></input>Admin
			</form>
			<button id="submitButton" name="submit" class="edit__button" type="button" >Submit</button>
		</div>
    </div>
</body>
</html>
