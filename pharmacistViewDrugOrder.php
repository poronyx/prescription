<?php 
    session_start();
    //redirect to log in page if not logged in
    if (!isset($_SESSION['loggedIN'])){
        
        header('Location: login.php');
        exit();
    }
    
    //logs out if tried to visit from wrong userType
    if($_SESSION['userType'] != "pharmacist"){
        header('Location: logout.php');
        exit();
    }

    if (!isset($_SESSION['pres_id'])){
        
        header('Location: login.php');
        exit();
    }

    if (isset($_POST['back'])){
            unset($_SESSION['dataArrayPresID']);
            unset($_SESSION['dataArrayQTY']);
            exit('back');
    }
    if (isset($_POST['add'])){
        $quantity = $_POST['prescribeQtyPHP'] ;  
        if ($quantity <= 0){
            exit('failed');
        }else{
            array_push( $_SESSION['dataArrayQTY'], $quantity);
            exit('added');
        }
        
    }
    if(isset($_POST['prescribe'])){
        $status = '';
        $connection = new mysqli('localhost', 'root', '','testestdb');
        for($x = 0;$x < sizeof($_SESSION['dataArrayPresID']);$x++){
            $quantity = $_SESSION['dataArrayQTY'][$x];
            $presOrderID = $_SESSION['dataArrayPresID'][$x];
            if($connection->query("UPDATE prescriptionhasorder SET quantity = $quantity
            WHERE prescription_order_id = $presOrderID;") === TRUE){
                $status = 'editSuccess';
                
            }else{
                $status = 'failed';
            }

            if($status == 'editSuccess'){
                $presID = $_SESSION['pres_id'];
                $connection->query("UPDATE prescription SET collection_status = 'COLLECTED'
                WHERE prescription_id = $presID;");
                $_SESSION['collection_status'] = 'COLLECTED';
            }
        }
        

        exit($status);
    }

    if(!isset($_SESSION['dataArrayPresID'])){

        $_SESSION['dataArrayPresID'] = array();
      }
      if(!isset($_SESSION['dataArrayQTY'])){

        $_SESSION['dataArrayQTY'] = array();
      }
    
    
    
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pharmacist</title>
    <link rel="stylesheet" href=".\css\main.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            console.log('page ready: patientHome');
            $("#backButton").on('click', function (){

                $.ajax(
                {
                    url: 'pharmacistViewDrugOrder.php',
                    method: 'POST',
                    data:{
                        back: 1
                    },
                    success: function(response){
                        $("#response").html(response);

                        if(response.indexOf('back') >= 0){

                            window.location = 'pharmacistViewPrescription.php';
                 
                        }else{
                            alert("Please try again");
                        }
             
                    },
                    dataType: 'text'
                }
                );
            });
            $("#submitButton").on('click', function (){
                var drugQTY = $("#td0").val();
                if(drugQTY == 0){
                    alert("Please fill in the quantity first!");
                }else{
                    $.ajax(
                    {
                        url: 'pharmacistViewDrugOrder.php',
                        method: 'POST',
                        data:{
                            prescribe: 1
                        },
                        success: function(response){
                            $("#response").html(response);

                            if(response.indexOf('editSuccess') >= 0){

                                window.location = 'pharmacistViewPrescription.php';
 
                            }else{
                                alert("Please try again");
                            }

                        },
                        dataType: 'text'
                    });
                }
                
            });
            

            $(".add__button").on('click', function (event){  
                var buttonClicked = event.target.id ;  
                var prescribeQty = $("#"+buttonClicked).val();
            $.ajax(
     {
         url: 'pharmacistViewDrugOrder.php',
         method: 'POST',
         data:{
             add: 1,
             prescribeQtyPHP: prescribeQty
         },
         success: function(response){
             $("#response").html(response);

             if(response.indexOf('added') >= 0){
                document.getElementById(buttonClicked).disabled = true;
                 
             }else{
                 alert("Please check your input again");
             }
             

             
         },
         dataType: 'text'
     }
);
});
        });

        </script>
    
    
</head>
<body>
    <nav>
        <img src="img\pharmacist.png" alt="pharmacist" style="width:7%">
        <ul>
            <li><a href="logout.php">Log out</a></li>
        </ul>
    </nav>
    <button type="button" class="back__button" id="backButton">Back</button>
    <div class="container">
        

        
        <div class="container__patient_prescription">
             <img src="img\clinic.png" alt="clinic" style="width:10%">
			<p>Wollongong Road 84 #01-34</p>
            <h1 align = "center" style = "font-size:25; font-family:Helvetica;">Health Clinic</h1>
			<p></p>
			<h1 align = "center" style = "font-size:25; font-family:Helvetica;">Original Prescription</h1>
			<br/><br/>
							
		    <p align = "left" style = "font-size:20; font-family:Helvetica;"> Name : <?php echo("{$_SESSION['patient_name']}"); ?> &emsp; Allergy : <?php echo("{$_SESSION['patient_allergy']}"); ?> </p>
            <p align = "left" style = "font-size:20; font-family:Helvetica;"> Address : <?php echo("{$_SESSION['patient_address']}"); ?> &emsp; DOB : <?php echo("{$_SESSION['patient_DOB']}"); ?> </p>
			<p align = "left" style = "font-size:20; font-family:Helvetica;"> Phone : <?php echo("{$_SESSION['patient_phone']}"); ?>&emsp; Gender : <?php echo("{$_SESSION['patient_gender']}"); ?> 
			&emsp; Weight(kg) : <?php echo("{$_SESSION['patient_weight']}"); ?> &emsp; Height(cm) : <?php echo("{$_SESSION['patient_height']}"); ?> </p>
            <br/><br/>

            <table>
                <th>Name</th>
                <th>Dose</th>
                <th>UOM</th>
                <th>Frequency</th>
                <th>Route</th>
                <th>Duration</th>
                <th>Special Instruction</th>
                <th>Quantity</th>
                <th></th>
                <?php 
                    $connection = new mysqli('localhost', 'root', '','testestdb');
                    $pres_id = $_SESSION['pres_id'];
    
                    $data = $connection->query("SELECT * FROM prescriptionhasorder where prescription_id = '$pres_id'");
                    
                    if($data->num_rows > 0){
                        $count = 0;
                        while($row = $data->fetch_assoc()){
                            $medID = $row['medicine_id'];
                            
                            $data2 = $connection->query("SELECT * FROM medicine where medicine_id = '$medID'");
                            if($data2->num_rows > 0){
                            while($row2 = $data2->fetch_assoc()){
                                $data3 = $connection->query("SELECT * FROM prescription WHERE prescription_id = '$pres_id'");
                                if($data3->num_rows > 0){
                                    while($row3 = $data3->fetch_assoc()){
                                        array_push( $_SESSION['dataArrayPresID'], $row['prescription_order_id']);
                                    echo "<tr><td>" . $row2['name'] ."</td><td>" . $row['dose']  .
                                    "</td><td>" .  $row['UOM'] ."</td><td>". $row['frequency'] ."</td><td>" .$row['Route'] ."</td><td>" .
                                    $row['stop_after']  ."</td><td>" .$row['drug_prescription']."</td><td>";
                                        if($row3['collection_status'] === 'NOT COLLECTED'){
                                            echo "<input class = 'td__box' id ='td".$count."' value='".$row['quantity']."'></input></td>
                                            <td><button type='button' class='add__button' id='td".$count."'>add</button></td></tr>";
                                        }else{
                                            echo "<input class = 'td__box' id ='td".$count."' value='".$row['quantity']."' disabled></input></td></tr>" ;
                                        }
                            $count = $count + 1;
                                    }
                                }
                            }
                        }else{
                                echo"</table><br><h1>No Records</h1>";
                            }
                        }
                        $_SESSION['countMedicines'] = $count;
                        if($_SESSION['collection_status'] === "NOT COLLECTED"){
                            echo"</table><br><button type='button' class='submit__button' id='submitButton'>Prescribe</button>";
                        }else{
                            echo"</table>";
                        }
                        
                    }else{
                        echo"</table><br><h1>No Records</h1>";
                    }   
                ?>
                <br/><br/>
                <p>Reviewed By: <?php echo("{$_SESSION['name']}"); ?></p>
                <p></p>
                <p align = "center" style = "font-size:15;">This is an electronically signed prescription;</p>
                <p></p>
                <p align = "center" style = "font-size:15;">Page 1 of 1</p>	
    </div>
    </div>
   
    
    
    
</body>
</html>

