<?php 
    session_start();
    //redirect to log in page if not logged in
    if (!isset($_SESSION['loggedIN'])){
        
        header('Location: login.php');
        exit();
    }
    
    //logs out if tried to visit from wrong userType
    if($_SESSION['userType'] != "pharmacist"){
        header('Location: logout.php');
        exit();
    }

    if (!isset($_SESSION['pres_id'])){
        
        header('Location: login.php');
        exit();
    }
    
    //Go back to DoctorHome page

    if (isset($_POST['back'])){
            unset($_SESSION['patient_id']);
            unset($_SESSION['doctor_id']);
            unset($_SESSION['date_pres']);
            
            exit('back');
    }
    
    
    
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pharmacist</title>
    <link rel="stylesheet" href=".\css\main.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            console.log('page ready: patientHome');
            $("#backButton").on('click', function (){

                $.ajax(
                {
                    url: 'pharmacistViewPrescription.php',
                    method: 'POST',
                    data:{
                        back: 1
                    },
                    success: function(response){
                        $("#response").html(response);

                        if(response.indexOf('back') >= 0){

                            window.location = 'pharmacistHome.php';
                 
                        }else{
                            alert("Please try again");
                        }
             
                    },
                    dataType: 'text'
                }
                );
            });
            

            
$("#viewButton").on('click', function (event){  


$.ajax(
    {
    url: 'pharmacistViewPrescription.php',
    method: 'POST',
    data:{
        view: 1
    },
    success: function(response){
        $("#response").html(response);

        if(response.indexOf('editSuccess') >= 0){
            window.location = 'pharmacistViewDrugOrder.php';
        }else{
            alert("Please try again");
        }



    },
    dataType: 'text'
    }
);
});
                
        });

        </script>
    
    
</head>
<body>
    <nav>
        <img src="img\pharmacist.png" alt="pharmacist" style="width:7%">
        <ul>
            <li><a href="logout.php">Log out</a></li>
        </ul>
    </nav>
    <button type="button" class="back__button" id="backButton">Back</button>
    <div class="container">
        

        <div class="container__patient_details">
        <?php 

                $connection = new mysqli('localhost', 'root', '','testestdb');
                $patientID = $_SESSION['patient_id'];
                $data = $connection->query("SELECT * FROM user,patient WHERE patient.patient_id = '$patientID' AND patient.user_id = user.user_id");
                
                if($data->num_rows > 0){
                    $row = $data->fetch_assoc();
                    $_SESSION['patient_id'] = $row['patient_id'];
                    $_SESSION['patient_name'] = $row['name'];
                    $_SESSION['patient_DOB'] = $row['DOB'];
                    $_SESSION['patient_email'] = $row['email'];
                    $_SESSION['patient_phone'] = $row['phone_number'];
                    $_SESSION['patient_weight'] = $row['weight'];
                    $_SESSION['patient_height'] = $row['height'];
                    $_SESSION['patient_gender'] = $row['gender'];
                    $_SESSION['patient_address'] = $row['address'];
                    $_SESSION['patient_allergy'] = $row['allergy'];
                }
                else{
                    echo"FAILED";
                }
                $doctorID = $_SESSION['doctor_id'];
                $data2 = $connection->query("SELECT * FROM user,doctor WHERE doctor.doctor_id = '$doctorID'AND doctor.user_id = user.user_id");
                if($data2->num_rows > 0){
                    $row2 = $data2->fetch_assoc();
                    $_SESSION['doc_name'] = $row2['name'];
                }
        
        ?>
            <h2 > Name : <?php echo("{$_SESSION['patient_name']}"); ?> </h2>
            <h2 > DOB : <?php echo("{$_SESSION['patient_DOB']}"); ?> </h2>
            <h2 > Email : <?php echo("{$_SESSION['patient_email']}"); ?> </h2>
            <h2 > Phone : <?php echo("{$_SESSION['patient_phone']}"); ?> </h2>
            <h2 > Weight(kg) : <?php echo("{$_SESSION['patient_weight']}"); ?> </h2>
            <h2 > Height(cm) : <?php echo("{$_SESSION['patient_height']}"); ?> </h2>
            <h2 > Gender : <?php echo("{$_SESSION['patient_gender']}"); ?> </h2>
            <h2 > Address : <?php echo("{$_SESSION['patient_address']}"); ?> </h2>
            <h2 > Allergy : <?php echo("{$_SESSION['patient_allergy']}"); ?> </h2>
        </div>
        <div class="container__patient_prescription">
            <?php 
            echo"<h3>Status: ".$_SESSION['collection_status'] ."</h3>";
            ?>
            <table>
                <th>Date</th>
                <th>Prescription ID</th>
                <th>Requested By</th>
                <th></th>
                <tr><td><?php echo"{$_SESSION['date_pres']}";?></td><td><?php echo"{$_SESSION['pres_id']}";?></td><td><?php echo"Dr.".$_SESSION['doc_name'];?></td>
                <td><button id="viewButton" class="view__button" type="button" >view</button></td></tr>
               
    </div>
    </div>
   
    
    
    
</body>
</html>
