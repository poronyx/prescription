<?php 
    session_start();
    //redirect to log in page if not logged in
    if (!isset($_SESSION['loggedIN']))
	{   
        header('Location: login.php');
        exit();
    }
    
    //logs out if tried to visit from wrong userType
    if($_SESSION['userType'] != "admin")
	{
        header('Location: logout.php');
        exit();
    }
    //logs out if tried to visit before searching for user
    if (!isset($_SESSION['user_id']))
	{
        header('Location: logout.php');
        exit();
    }
    //Go back to AdminHome page
    if (isset($_POST['back']))
	{
		unset($_SESSION['user_user_id']);
		unset($_SESSION['user_password']);
		unset($_SESSION['user_NRIC']);
		unset($_SESSION['user_name']);
		unset($_SESSION['user_DOB']);
		unset($_SESSION['user_email']);
		unset($_SESSION['user_phone']);
		unset($_SESSION['user_role']);
		if($_SESSION['user_role'] == 'doctor')
		{
			unset($_SESSION['doctor_doctor_id']);
			unset($_SESSION['doctor_license']);
		}
		elseif($_SESSION['user_role'] == 'patient')
		{
			unset($_SESSION['patient_patient_id']);
			unset($_SESSION['patient_weight']);
			unset($_SESSION['patient_height']);
			unset($_SESSION['patient_gender']);
			unset($_SESSION['patient_address']);
			unset($_SESSION['patient_allergy']);
		}
		elseif($_SESSION['user_role'] == 'pharmacist')
		{
			unset($_SESSION['pharmacist_pharmacist_id']);
			unset($_SESSION['pharmacist_license']);
		}
		else
		{
			unset($_SESSION['admin_admin_id']);
		}
		exit('back');
    }
	if (isset($_POST['view']))
	{
		
		
	}	
	
    if (isset($_POST['add']))
	{    
        exit('adding');
	}
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
    <link rel="stylesheet" href=".\css\main.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            console.log('page ready: adminUserList');

            $("#backButton").on('click', function (){
				$.ajax({
					url: 'adminUserList.php',
					method: 'POST',
					data:{
						back: 1
					},
					success: function(response){
						$("#response").html(response);

						if(response.indexOf('back') >= 0){

							window.location = 'adminHome.php';
							
						}else{
							alert("Please try again");
						}
					},dataType: 'text'
				});
            });
			
        });

        </script>
    
    
</head>
<body>
	<header>
		<nav>
			<img src="img\admin-with-cogwheels.png" alt="admin" style="width:7%; float: left;">
			<div class="nav__name"><h2><?php echo("{$_SESSION['name']}"); ?></h2></div>
        <ul>
            <li><a href="logout.php">Log out</a></li>
        </ul>
		</nav>
	</header>
	
	<div>
		<button type="button" class="back__button" id="backButton">Back</button>
    </div>
	</br></br>
    <div class="container" style="width: 50%; margin-left: 20%;">
        <div class="container__user_list">
            <table class = "table__add">
                <th style="width: 20%;">User ID</th>
                <th style="width: 20%;">NRIC</th>
                <th style="width: 20%;">Name</th>
                <th style="width: 20%;">Role</th>
                <th style="width: 10%;"></th> 
                <th style="width: 10%;"></th> 
                <tr>
                    <td><?php echo("{$_SESSION['user_user_id']}"); ?></td>
                    <td><?php echo("{$_SESSION['user_NRIC']}"); ?></td>
					<td><?php echo("{$_SESSION['user_name']}"); ?></td>
					<td><?php echo("{$_SESSION['user_role']}"); ?> </td>
					<td><button type="button" class="view__button" id="viewButton"><a href="adminViewAccount.php">View</a></button></td>
					<td><button type="button" class="update__button" id="updateButton"><a href="adminEditAccount.php">Update</a></button></td>
                </tr>  
            </table>
		</div>
    </div>

       