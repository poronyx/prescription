<?php 
    session_start();
    //redirect to log in page if not logged in
    if (!isset($_SESSION['loggedIN'])){
        
        header('Location: login.php');
        exit();
    }
    
    //logs out if tried to visit from wrong userType
    if($_SESSION['userType'] != "doctor"){
        header('Location: logout.php');
        exit();
    }
    //logs out if tried to visit before searching for patient
    if (!isset($_SESSION['patient_id'])){
        
        header('Location: logout.php');
        exit();
    }
    //Go back to DoctorHome page
    if (isset($_POST['back'])){
            unset($_SESSION['pres_id']);
            
            exit('back');
    }

    if (isset($_POST['edit'])){
        $_SESSION['edit_id'] = $_POST['editPresIDPHP']; 
        exit('editSuccess');
}
    
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Doctor</title>
    <link rel="stylesheet" href=".\css\main.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            console.log('page ready: DoctorHasPatient');

            $("#backButton").on('click', function (){

               $.ajax(
                    {
                        url: 'doctorViewPrescription.php',
                        method: 'POST',
                        data:{
                            back: 1
                        },
                        success: function(response){
                            $("#response").html(response);

                            if(response.indexOf('back') >= 0){

                                window.location = 'doctorHasPatient.php';
                                
                            }else{
                                alert("Please try again");
                            }
                            
                        },
                        dataType: 'text'
                    }
               );
            });
            // edit button for updating prescription
            $(".edit__button--prescription").on('click', function (event){  
                var buttonClicked = event.target.id ;  
                var editPresID = $("#"+buttonClicked).val();
                console.log(editPresID);
            $.ajax(
     {
         url: 'doctorViewPrescription.php',
         method: 'POST',
         data:{
             edit: 1,
             editPresIDPHP: editPresID
         },
         success: function(response){
             $("#response").html(response);

             if(response.indexOf('editSuccess') >= 0){

                window.location = 'doctorEditPrescription.php';
                 
             }else{
                 alert("Please try again");
             }
             

             
         },
         dataType: 'text'
     }
);
});
                
        });

        </script>
    
    
</head>
<body>
    <nav>
        <img src="img\doctor.png" alt="doctor" style="width:7%">
        <div class="nav__name"><h2>Dr. <?php echo("{$_SESSION['name']}"); ?></h2></div>
        <ul>
            <li><a href="logout.php">Log out</a></li>
        </ul>
    </nav>

    <button type="button" class="back__button" id="backButton">Back</button>
    <div class="container">
        
		
		
        
        <div class="container__view_prescription">
            <img src="img\clinic.png" alt="clinic" style="width:10%">
			<p>Wollongong Road 84 #01-34</p>
            <h1 align = "center" style = "font-size:25; font-family:Helvetica;">Health Clinic</h1>
			<p></p>
			<h1 align = "center" style = "font-size:25; font-family:Helvetica;">Original Prescription</h1>
			<br/><br/>
							
		    <p align = "left" style = "font-size:20; font-family:Helvetica;"> Name : <?php echo("{$_SESSION      ['patient_name']}"); ?> &emsp; Allergy : <?php echo("{$_SESSION['patient_allergy']}"); ?> </p>
            <p align = "left" style = "font-size:20; font-family:Helvetica;"> Address : <?php echo("{$_SESSION['patient_address']}"); ?> &emsp; DOB : <?php echo("{$_SESSION['patient_DOB']}"); ?> </p>
			<p align = "left" style = "font-size:20; font-family:Helvetica;"> Phone : <?php echo("{$_SESSION['patient_phone']}"); ?>&emsp; Gender : <?php echo("{$_SESSION['patient_gender']}"); ?> 
			&emsp; Weight(kg) : <?php echo("{$_SESSION['patient_weight']}"); ?> &emsp; Height(cm) : <?php echo("{$_SESSION['patient_height']}"); ?> </p>
            <br/><br/>
            <table>
                <th>Name</th>
                <th>Dose</th>
                <th>UOM</th>
                <th>Frequency</th>
                <th>Route</th>
                <th>Duration</th>
                <th>Special instruction</th>
                <th><th>

            <?php
                $connection = new mysqli('localhost', 'root', '','testestdb');
                $pres_id = $_SESSION['pres_id'];

                $getMedicine = $connection->query("SELECT * FROM prescriptionhasorder where prescription_id = '$pres_id'");
                
                if($getMedicine->num_rows > 0){
                    $count = 0;
                    while($row = $getMedicine->fetch_assoc()){
                        $medID = $row['medicine_id'];
                        
                        $getMedicine2 = $connection->query("SELECT * FROM medicine where medicine_id = '$medID'");
                        if($getMedicine2->num_rows > 0){
                        while($row2 = $getMedicine2->fetch_assoc()){
                            $getPrescription = $connection->query("SELECT * FROM prescription WHERE prescription_id = '$pres_id'");
                            if($getPrescription->num_rows > 0){
                                while($row3 = $getPrescription->fetch_assoc()){

                                echo "<tr><td>" . $row2['name'] ."</td><td>" . $row['dose']  .
                                "</td><td>" .  $row['UOM'] ."</td><td>". $row['frequency'] ."</td><td>" .$row['Route'] ."</td><td>" .
                                $row['stop_after']  ."</td><td>" .$row['drug_prescription']."<td>";
                                    if($row3['collection_status'] === 'NOT COLLECTED' && $row3['doctor_id'] === $_SESSION['doctor_id']){
                                        echo "<button type='button' class='edit__button--prescription' id='editPrescriptionButton".$count
                                        ."' value='".$row['prescription_order_id']."'>Edit</button></td></td></tr>" ;
                                    }else{
                                        echo "</td></td></tr>" ;
                                    }
                                
                         
                                
                        $count = $count + 1;
                                }
                            }
                        }
                    }else{
                            echo"</table><br><h1>No Records</h1>";
                        }
                }
                    echo"</table>";
                }else{
                    echo"</table><br><h1>No Records</h1>";
                }   
            ?> 
            <br/><br/>
            <p>Doctor's Name: <?php echo("{$_SESSION['name']}"); ?></p>
            <p></p>
            <p align = "center" style = "font-size:15;">This is an electronically signed prescription;</p>
            <p></p>
            <p align = "center" style = "font-size:15;">Page 1 of 1</p>   
    </div>
    </div>
   
    
    
    
</body>
</html>

